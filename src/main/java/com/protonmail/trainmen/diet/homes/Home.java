package com.protonmail.trainmen.diet.homes;

import com.google.common.collect.Maps;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.Map;

public class Home implements ConfigurationSerializable
{
	private String name;
	private Location location;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Location getLocation()
	{
		return location;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	@Override
	public Map<String, Object> serialize()
	{
		Map<String, Object> data = Maps.newHashMap();
		data.put("name", name);
		data.put("location", location);
		return data;
	}

	public static Home deserialize(Map<String, Object> data)
	{
		Home home = new Home();
		home.setName((String) data.get("name"));
		home.setLocation((Location) data.get("location"));
		return home;
	}
}
