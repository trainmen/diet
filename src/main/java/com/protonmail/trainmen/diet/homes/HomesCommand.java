package com.protonmail.trainmen.diet.homes;

import com.protonmail.trainmen.diet.Message;
import com.protonmail.trainmen.diet.data.DietPlayer;
import com.protonmail.trainmen.diet.data.UserManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class HomesCommand implements CommandExecutor
{
	private final UserManager userManager;

	public HomesCommand(UserManager userManager)
	{
		this.userManager = userManager;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (!sender.hasPermission("diet.homes"))
		{
			sender.sendMessage(Message.newNoPermission(label));
			return false;
		}

		if (!(sender instanceof Player))
		{
			sender.sendMessage(Message.newPlayerOnly(label));
			return false;
		}

		Player player = (Player) sender;
		DietPlayer dietPlayer = userManager.getPlayerById(player.getUniqueId());

		if (dietPlayer.getHomes().size() == 0)
		{
			player.sendMessage(Message.newError("You don't have any homes. Use /sethome to create one."));
		}
		else
		{
			List<String> homeNames = dietPlayer.getHomes().stream()
					.map(home -> home.getName())
					.collect(Collectors.toList());
			String homes = String.join(", ", homeNames);
			player.sendMessage(Message.newSuccess("You currently have the homes: %s.", homes));
		}

		return true;
	}
}
