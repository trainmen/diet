package com.protonmail.trainmen.diet.homes;

import com.protonmail.trainmen.diet.Message;
import com.protonmail.trainmen.diet.data.DietPlayer;
import com.protonmail.trainmen.diet.data.UserManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DelHomeCommand implements CommandExecutor
{
	private final UserManager userManager;

	public DelHomeCommand(UserManager userManager)
	{
		this.userManager = userManager;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (!sender.hasPermission("diet.delhome"))
		{
			sender.sendMessage(Message.newNoPermission(label));
			return false;
		}

		if (!(sender instanceof Player))
		{
			sender.sendMessage(Message.newPlayerOnly(label));
			return false;
		}

		Player player = (Player) sender;
		DietPlayer dietPlayer = userManager.getPlayerById(player.getUniqueId());
		String homeName = args.length == 0 ? "home" : args[0];

		Home home = dietPlayer.getHomes().stream()
				.filter(home1 -> home1.getName().equalsIgnoreCase(homeName))
				.findFirst().orElse(null);

		if (home == null)
		{
			sender.sendMessage(Message.newError("Could not find your home. Create one with /sethome [name]"));
			return false;
		}

		player.sendMessage(Message.newSuccess("Home has been deleted."));
		dietPlayer.getHomes().remove(home);
		return true;
	}
}
