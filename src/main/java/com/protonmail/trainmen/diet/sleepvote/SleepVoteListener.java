package com.protonmail.trainmen.diet.sleepvote;

import com.google.common.collect.Sets;
import com.protonmail.trainmen.diet.Message;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Set;
import java.util.UUID;

public class SleepVoteListener implements Listener
{
	private final Set<UUID> sleeping = Sets.newHashSet();

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBedEnter(PlayerBedEnterEvent bedEnterEvent)
	{
		if (!bedEnterEvent.isCancelled() && bedEnterEvent.useBed() != Event.Result.DENY)
		{
			if (sleeping.add(bedEnterEvent.getPlayer().getUniqueId()))
			{
				Bukkit.broadcastMessage(Message.newInfo("%s is now sleeping! (%d/%d)", bedEnterEvent.getPlayer().getName(), sleeping.size(), getRequiredSleepers()));
				if (sleeping.size() >= getRequiredSleepers())
				{
					bedEnterEvent.getPlayer().getWorld().setTime(1000);
					sleeping.clear();
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBedLeave(PlayerBedLeaveEvent bedLeaveEvent)
	{
		sleeping.remove(bedLeaveEvent.getPlayer().getUniqueId());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent quitEvent)
	{
		sleeping.remove(quitEvent.getPlayer().getUniqueId());
	}

	public int getRequiredSleepers()
	{
		return (int) Math.round(0.5 * Bukkit.getOnlinePlayers().size());
	}
}
