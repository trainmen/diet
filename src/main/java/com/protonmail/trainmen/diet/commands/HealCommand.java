package com.protonmail.trainmen.diet.commands;

import com.protonmail.trainmen.diet.Message;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HealCommand implements CommandExecutor
{
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (!sender.hasPermission("diet.heal"))
		{
			sender.sendMessage(Message.newNoPermission(label));
			return false;
		}

		if (!(sender instanceof Player))
		{
			sender.sendMessage(Message.newPlayerOnly(label));
			return false;
		}

		Player target = (Player) sender;

		if (args.length >= 1)
		{
			target = Bukkit.getPlayer(args[0]);

			if (target == null)
			{
				sender.sendMessage(Message.newNoSuchPlayer(args[0]));
				return false;
			}
		}

		sender.sendMessage(Message.newSuccess("%s has been healed.", target.getName()));
		target.setHealth(target.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
		return true;
	}
}
