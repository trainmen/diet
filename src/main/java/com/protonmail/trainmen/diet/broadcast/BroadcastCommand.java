package com.protonmail.trainmen.diet.broadcast;

import com.protonmail.trainmen.diet.Message;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BroadcastCommand implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (!sender.hasPermission("diet.broadcast"))
		{
			sender.sendMessage(Message.newNoPermission(label));
			return false;
		}

		if (args.length == 0)
		{
			sender.sendMessage(Message.newNotEnoughArgs(label, 1));
			return false;
		}

		String message = String.join(" ", args);
		Bukkit.getServer().broadcastMessage(Message.newColored(ChatColor.GOLD, "Broadcast: %s", message));
		return true;
	}
}
