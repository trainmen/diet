package com.protonmail.trainmen.diet.tpa;

import java.util.UUID;

public class Request
{
	private UUID id;
	private UUID sender;
	private UUID recipient;
	private long timestamp;
	private boolean isTpHere;

	public UUID getId()
	{
		return id;
	}

	public void setId(UUID id)
	{
		this.id = id;
	}

	public UUID getSender()
	{
		return sender;
	}

	public void setSender(UUID sender)
	{
		this.sender = sender;
	}

	public UUID getRecipient()
	{
		return recipient;
	}

	public void setRecipient(UUID recipient)
	{
		this.recipient = recipient;
	}

	public long getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(long timestamp)
	{
		this.timestamp = timestamp;
	}

	public boolean isTpHere()
	{
		return isTpHere;
	}

	public void setTpHere(boolean tpHere)
	{
		isTpHere = tpHere;
	}
}
