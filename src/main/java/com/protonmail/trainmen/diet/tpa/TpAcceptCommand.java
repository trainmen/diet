package com.protonmail.trainmen.diet.tpa;

import com.protonmail.trainmen.diet.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

public class TpAcceptCommand implements CommandExecutor
{
	private final RequestManager requestManager;

	public TpAcceptCommand(RequestManager requestManager)
	{
		this.requestManager = requestManager;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (!sender.hasPermission("diet.tpaccept"))
		{
			sender.sendMessage(Message.newNoPermission(label));
			return false;
		}

		if (!(sender instanceof Player))
		{
			sender.sendMessage(Message.newPlayerOnly(label));
			return false;
		}

		if (args.length == 0)
		{
			sender.sendMessage(Message.newNotEnoughArgs(label, 1));
			return false;
		}

		Player player = (Player) sender;
		Player requestSender = Bukkit.getPlayer(args[0]);

		if (requestSender == null)
		{
			sender.sendMessage(Message.newNoSuchPlayer(args[0]));
			return false;
		}

		if (player == requestSender)
		{
			player.sendMessage(Message.newError("You can not send a teleport request to yourself."));
			return false;
		}

		Optional<Request> requestOpt = requestManager.getRequestByPair(requestSender.getUniqueId(), player.getUniqueId());

		if (!requestOpt.isPresent())
		{
			player.sendMessage(Message.newError("You don't have an active request from %s.", requestSender.getName()));
			return false;
		}

		Request request = requestOpt.get();
		requestManager.removeRequest(request);

		if (request.isTpHere())
		{
			requestSender.sendMessage(Message.newInfo("%s has been teleported to you.", player.getName()));
			player.sendMessage(Message.newSuccess("You have been teleported to %s", requestSender.getName()));
			player.teleport(requestSender);
		}
		else
		{
			player.sendMessage(Message.newInfo("%s has been teleported to you.", requestSender.getName()));
			requestSender.sendMessage(Message.newSuccess("You have been teleported to %s", player.getName()));
			requestSender.teleport(player);
		}
		return true;
	}
}
