package com.protonmail.trainmen.diet.tpa;

import com.protonmail.trainmen.diet.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class TpaCommand implements CommandExecutor
{
	private final RequestManager requestManager;

	public TpaCommand(RequestManager requestManager)
	{
		this.requestManager = requestManager;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (!sender.hasPermission("diet.tpa"))
		{
			sender.sendMessage(Message.newNoPermission(label));
			return false;
		}

		if (!(sender instanceof Player))
		{
			sender.sendMessage(Message.newPlayerOnly(label));
			return false;
		}

		if (args.length == 0)
		{
			sender.sendMessage(Message.newNotEnoughArgs(label, 1));
			return false;
		}

		Player player = (Player) sender;
		Player recipient = Bukkit.getPlayer(args[0]);

		if (recipient == null)
		{
			player.sendMessage(Message.newNoSuchPlayer(args[0]));
			return false;
		}

		if (player == recipient)
		{
			player.sendMessage(Message.newError("You can not send a teleport request to yourself."));
			return false;
		}

		if (requestManager.getRequestByPair(player.getUniqueId(), recipient.getUniqueId()).isPresent())
		{
			sender.sendMessage(Message.newError("You already have an active request to %s.", recipient.getName()));
			return false;
		}

		Request request = new Request();
		request.setId(UUID.randomUUID());
		request.setSender(player.getUniqueId());
		request.setRecipient(recipient.getUniqueId());
		request.setTimestamp(System.currentTimeMillis());
		request.setTpHere(false);
		requestManager.addRequest(request);

		player.sendMessage(Message.newSuccess("A request has been sent to %s.", recipient.getName()));
		recipient.sendMessage(Message.newInfo("%s would like to teleport to you. Type /tpaccept %1$s to accept.", player.getName()));
		return true;
	}
}
