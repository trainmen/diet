package com.protonmail.trainmen.diet.tpa;

import java.util.*;
import java.util.stream.Collectors;

public class RequestManager
{
	private final Set<Request> requests = new HashSet<>();

	public boolean addRequest(Request request)
	{
		return requests.add(request);
	}

	public Optional<Request> getRequestById(UUID requestId)
	{
		return requests.stream()
				.filter(request -> request.getId().equals(requestId))
				.findFirst();
	}

	public List<Request> getRequestsBySender(UUID senderId)
	{
		return requests.stream()
				.filter(request -> request.getSender().equals(senderId))
				.collect(Collectors.toList());
	}

	public List<Request> getRequestsByRecipient(UUID recipientId)
	{
		return requests.stream()
				.filter(request -> request.getRecipient().equals(recipientId))
				.collect(Collectors.toList());
	}

	public Optional<Request> getRequestByPair(UUID senderId, UUID recipientId)
	{
		return requests.stream()
				.filter(request -> request.getSender().equals(senderId) && request.getRecipient().equals(recipientId))
				.findFirst();
	}

	public boolean removeRequest(Request request)
	{
		return requests.remove(request);
	}
}
