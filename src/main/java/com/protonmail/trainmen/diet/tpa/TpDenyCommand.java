package com.protonmail.trainmen.diet.tpa;

import com.protonmail.trainmen.diet.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

public class TpDenyCommand implements CommandExecutor
{
	private final RequestManager requestManager;

	public TpDenyCommand(RequestManager requestManager)
	{
		this.requestManager = requestManager;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (!sender.hasPermission("diet.tpdeny"))
		{
			sender.sendMessage(Message.newNoPermission(label));
			return false;
		}

		if (!(sender instanceof Player))
		{
			sender.sendMessage(Message.newPlayerOnly(label));
			return false;
		}

		if (args.length == 0)
		{
			sender.sendMessage(Message.newNotEnoughArgs(label, 1));
			return false;
		}

		Player player = (Player) sender;
		Player requestSender = Bukkit.getPlayer(args[0]);

		if (requestSender == null)
		{
			sender.sendMessage(Message.newNoSuchPlayer(args[0]));
			return false;
		}

		Optional<Request> requestOpt = requestManager.getRequestByPair(requestSender.getUniqueId(), player.getUniqueId());

		if (!requestOpt.isPresent())
		{
			player.sendMessage(Message.newError("You don't have an active request from %s.", requestSender.getName()));
			return false;
		}

		Request request = requestOpt.get();
		requestManager.removeRequest(request);

		sender.sendMessage(Message.newInfo("You have declined %ss request.", requestSender.getName()));
		requestSender.sendMessage(Message.newError("%s declined your request.", sender.getName()));
		return true;
	}
}
