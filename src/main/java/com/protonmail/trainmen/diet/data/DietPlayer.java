package com.protonmail.trainmen.diet.data;

import com.google.common.collect.Lists;
import com.protonmail.trainmen.diet.homes.Home;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;
import java.util.UUID;

public class DietPlayer
{
	private final FileConfiguration data;

	public DietPlayer(FileConfiguration data)
	{
		this.data = data;
	}

	public FileConfiguration getData()
	{
		return data;
	}

	public UUID getId()
	{
		return UUID.fromString(data.getString("id"));
	}

	public void setId(UUID id)
	{
		data.set("id", id.toString());
	}

	public String getName()
	{
		return data.getString("name");
	}

	public void setName(String name)
	{
		data.set("name", name);
	}

	public List<Home> getHomes()
	{
		if (!data.contains("homes"))
		{
			data.set("homes", Lists.newArrayList());
		}
		return (List<Home>) data.getList("homes");
	}

	public void setHomes(List<Home> homes)
	{
		data.set("homes", homes);
	}
}
