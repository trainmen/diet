package com.protonmail.trainmen.diet.data;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class UserDataListener implements Listener
{
	private final UserManager userManager;

	public UserDataListener(UserManager userManager)
	{
		this.userManager = userManager;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPreLogin(AsyncPlayerPreLoginEvent preLoginEvent)
	{
		if (preLoginEvent.getLoginResult() == AsyncPlayerPreLoginEvent.Result.ALLOWED)
		{
			userManager.getPlayerById(preLoginEvent.getUniqueId());
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent joinEvent)
	{
		DietPlayer dietPlayer = userManager.getPlayerById(joinEvent.getPlayer().getUniqueId());
		dietPlayer.setName(joinEvent.getPlayer().getName());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event)
	{
		userManager.savePlayerById(event.getPlayer().getUniqueId());
	}
}
