package com.protonmail.trainmen.diet.data;

import com.google.common.collect.Maps;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

public class UserManager
{
	private final Map<UUID, DietPlayer> players = Maps.newConcurrentMap();
	private final File dataFolder;

	public UserManager(Plugin plugin)
	{
		dataFolder = new File(plugin.getDataFolder(), "players");
		dataFolder.mkdirs();
	}

	public DietPlayer getPlayerById(UUID playerId)
	{
		DietPlayer existingPlayer = players.get(playerId);

		if (existingPlayer != null)
		{
			return existingPlayer;
		}

		DietPlayer dietPlayer = new DietPlayer(new YamlConfiguration());
		File playerDataFile = new File(dataFolder, playerId + ".yml");

		try
		{
			if (playerDataFile.exists())
			{
				dietPlayer.getData().load(playerDataFile);
			}
			else
			{
				playerDataFile.createNewFile();
				dietPlayer.setId(playerId);
			}
		}
		catch (IOException | InvalidConfigurationException e)
		{
			e.printStackTrace();
		}

		players.put(playerId, dietPlayer);
		return dietPlayer;
	}

	public void savePlayer(DietPlayer dietPlayer)
	{
		File playerDataFile = new File(dataFolder, dietPlayer.getId() + ".yml");
		try
		{
			if (!playerDataFile.exists())
			{
				playerDataFile.createNewFile();
			}
			dietPlayer.getData().save(playerDataFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void savePlayerById(UUID playerId)
	{
		savePlayer(getPlayerById(playerId));
	}
}
