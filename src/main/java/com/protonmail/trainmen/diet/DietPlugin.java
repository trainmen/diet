package com.protonmail.trainmen.diet;

import com.protonmail.trainmen.diet.broadcast.BroadcastCommand;
import com.protonmail.trainmen.diet.commands.FeedCommand;
import com.protonmail.trainmen.diet.commands.HealCommand;
import com.protonmail.trainmen.diet.data.UserDataListener;
import com.protonmail.trainmen.diet.data.UserManager;
import com.protonmail.trainmen.diet.homes.*;
import com.protonmail.trainmen.diet.sleepvote.SleepVoteListener;
import com.protonmail.trainmen.diet.tpa.*;
import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class DietPlugin extends JavaPlugin
{
	private final RequestManager requestManager = new RequestManager();
	private final UserManager userManager = new UserManager(this);

	@Override
	public void onEnable()
	{
		Bukkit.getPluginManager().registerEvents(new UserDataListener(userManager), this);
		Bukkit.getPluginManager().registerEvents(new SleepVoteListener(), this);
		ConfigurationSerialization.registerClass(Home.class, "Home");
		getCommand("broadcast").setExecutor(new BroadcastCommand());
		getCommand("tpa").setExecutor(new TpaCommand(requestManager));
		getCommand("tpahere").setExecutor(new TpaHereCommand(requestManager));
		getCommand("tpaccept").setExecutor(new TpAcceptCommand(requestManager));
		getCommand("tpdeny").setExecutor(new TpDenyCommand(requestManager));
		getCommand("home").setExecutor(new HomeCommand(userManager));
		getCommand("sethome").setExecutor(new SetHomeCommand(userManager));
		getCommand("delhome").setExecutor(new DelHomeCommand(userManager));
		getCommand("homes").setExecutor(new HomesCommand(userManager));
		getCommand("heal").setExecutor(new HealCommand());
		getCommand("feed").setExecutor(new FeedCommand());
	}

	@Override
	public void onDisable()
	{
		for (Player player : Bukkit.getOnlinePlayers())
		{
			userManager.savePlayerById(player.getUniqueId());
		}
	}

	public RequestManager getRequestManager()
	{
		return requestManager;
	}

	public UserManager getUserManager()
	{
		return userManager;
	}
}
